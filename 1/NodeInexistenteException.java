public class NodeInexistenteException extends Exception{

	public NodeInexistenteException(String s) {
		super(s);
	}
	
	public NodeInexistenteException(){
	}
}
