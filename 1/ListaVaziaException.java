public class ListaVaziaException extends Exception {
	public ListaVaziaException(String s) {
		super(s);
	}
	
	public ListaVaziaException(){
	}
}
