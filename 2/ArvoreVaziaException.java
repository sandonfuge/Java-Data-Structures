package exceptions;

public class ArvoreVaziaException extends Exception {
	public ArvoreVaziaException(String s){
		super(s);
	}
}
