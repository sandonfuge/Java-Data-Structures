package exceptions;

public class NaoExisteException extends Exception{
	public NaoExisteException(String s){
		super(s);
	}
}
